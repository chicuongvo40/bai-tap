﻿int soLuongQuang, soLuongXuVang;

// Nhập số lượng mảnh quặng từ người dùng
Console.Write("Nhập số mảnh quặng đã thu thập: ");
soLuongQuang = Convert.ToInt32(Console.ReadLine());

// Tính số lượng đồng xu vàng dựa trên công thức toán học
soLuongXuVang = 10 * Math.Min(10, soLuongQuang);
soLuongXuVang += 5 * Math.Min(5, Math.Max(0, soLuongQuang - 10));
soLuongXuVang += 2 * Math.Min(3, Math.Max(0, soLuongQuang - 15));
soLuongXuVang += Math.Max(0, soLuongQuang - 18);

// Hiển thị số lượng đồng xu vàng
Console.WriteLine("Số lượng đồng xu vàng mà bạn nhận được là: {0}", soLuongXuVang);

Console.ReadLine(); // Dừng màn hình console